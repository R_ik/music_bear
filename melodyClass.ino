#include "note_struct.h"
#include "rct.h"
#include "mary.h"
#include "danube.h"
#include "croc.h"
#include "aoe.h"
#include "row_row.h"
#include "cowboy.h"
#include "aoe2_ingame.h"
#include "avr/sleep.h"

#define TONE_PIN 3
#define BUTTON_PIN 2
//#define SERIAL_DEBUG
#define DEBOUNCE_MILLIS 600

volatile unsigned int stop_song = 0;

void setup() {
  randomSeed(34176543);
  powerSaving();
  pinMode(BUTTON_PIN, INPUT_PULLUP);
#ifdef SERIAL_DEBUG
  Serial.begin(9600); delay(10);
  Serial.println("Started");
#endif
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), digitalInterrupt, FALLING); 
  delay(100);
}

void PROGMEM_readNote(const struct note * sce, struct note& dest){
  memcpy_P (&dest, sce, sizeof (note));
}

void midi() {
  int selected_song = random(1,9); 
  const note* notes_list PROGMEM;
  int n_notes;
  unsigned long start_time = millis();
  switch(selected_song){
    case 1:
      notes_list = rct;
      n_notes = sizeof(rct) / sizeof(note);
      break;
    case 2:
      notes_list = mary;
      n_notes = sizeof(mary) / sizeof(note);
      break;
    case 3:
      notes_list = danube;
      n_notes = sizeof(danube) / sizeof(note);
      break;
    case 4:
      notes_list = croc;
      n_notes = sizeof(croc) / sizeof(note);
      break;
    case 5:
      notes_list = aoe;
      n_notes = sizeof(aoe) / sizeof(note);
      break;
    case 6:
      notes_list = row_row;
      n_notes = sizeof(row_row) / sizeof(note);
      break;
    case 7:
      notes_list = cowboy;
      n_notes = sizeof(cowboy) / sizeof(note);
      break;
    case 8:
      notes_list = aoe2_ingame;
      n_notes = sizeof(aoe2_ingame) / sizeof(note);
      break;
  }
  note currentNote;
  stop_song = 0;
  for(int i = 0; i < n_notes; i++) {
    PROGMEM_readNote(&notes_list[i], currentNote);
    tone(TONE_PIN, currentNote.freq, currentNote.duration);
    delay(currentNote.pause);
    if(stop_song) {
      if(millis() - start_time < DEBOUNCE_MILLIS) {
        stop_song = 0;
      } else {
        break;
      }
    }
  }
  noTone(TONE_PIN);
}

void loop() {
    stop_song = 0;
    midi();
#ifdef SERIAL_DEBUG
    Serial.println("Button pressed");
#endif
    delay(500);
    goSleep();
}

void powerSaving() {
  // Set all pins to input mode to save power
  for (int i = 4; i < 20; i++) {
    pinMode(i, OUTPUT);
  }
  //Disable ADC
  ADCSRA &= ~(1 << 7);
}

void digitalInterrupt(){
  sleep_disable();
  stop_song = 1;
}

void goSleep(){  
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_bod_disable(); 
  sleep_cpu();
}
